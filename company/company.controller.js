const express = require('express')
const router = express.Router()
const Joi = require('joi')
const validateRequest = require('_middleware/validate-request');
const authorize = require('../_middleware/authorize');
const companyService = require('./company.service');

// routes
router.post('/add', authorize(), addCompanySchema, addCompany)

module.exports = router;

function addCompanySchema(req, res, next) {
    const schema = Joi.object({
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        namaPerusahaan: Joi.string().required(),
        tanggalBerdiri: Joi.string().required(),
        tipePerusahaan: Joi.string().required(),
        biayaPendaftaran: Joi.number()
    })
    validateRequest(req, next, schema)
}

function addCompany(req, res, next) {
    companyService.create(req.body)
        .then(() => res.json({message: "Your company added successfully"}))
        .catch(next)
        .catch(err=> console.log(err))
}
