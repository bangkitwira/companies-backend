'use strict';

var response = require('./res');
var connection = require('./conn');

exports.users = function (req, res) {
    connection.query('SELECT id, username, created_at FROM user', function (error, rows, fields) {
        if (error) {
            console.log(error)
        } else {
            response.ok(rows, res)
        }
    });
};

exports.login = function (req, res) {
    const username = req.body.username
    console.log(username)
    connection.query('SELECT * FROM user WHERE username = ? AND password = ?', [username, password], function (error, rows, fields) {
        if (error) {
            console.log(error)
        } else {
            response.ok(rows, res)
        }
    });
};

exports.index = function (req, res) {
    response.ok("Hello from the Node JS RESTful side!", res)
};