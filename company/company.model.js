const { DataTypes } = require("sequelize")

module.exports = model

function model(sequelize) {
    const attributes = {
        firstName: { type: DataTypes.STRING, allowNull: false},
        lastName: { type: DataTypes.STRING, allowNull: false},
        namaPerusahaan: { type: DataTypes.STRING, allowNull: false},
        tanggalBerdiri: { type: DataTypes.DATE, allowNull: false},
        tipePerusahaan: { type: DataTypes.STRING, allowNull: false},
        lateStatus: { type: DataTypes.STRING, allowNull: true},
        biayaPendaftaran: { type: DataTypes.INTEGER, allowNull: true}
    }

    return sequelize.define('Company', attributes)
}