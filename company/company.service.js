const config = require('config.json')
const db = require('_helpers/db')
const luxon = require('luxon')

module.exports = {
    create
}

async function create(params) {

    const startDate = luxon.DateTime.fromISO(params.tanggalBerdiri).set({
        year: luxon.DateTime.now().get("year")
    });
    const diff = startDate.diffNow().as('day');

    if (params.tipePerusahaan === 'perseorangan') {
        params = { ...params, biayaPendaftaran: 1000000 }
        if (diff > 30) {
            params = { ...params, lateStatus: "TERLAMBAT" }
        }
    } else {
        params = { ...params, biayaPendaftaran: 3000000 }
        if (diff > 30) {
            params = { ...params, lateStatus: "TERLAMBAT" }
        }
    }
    console.log(params)
    // save company
    await db.Company.create(params);
}